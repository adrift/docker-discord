FROM ubuntu:18.10

ENV QT_XKB_CONFIG_ROOT=/usr/share/X11/xkb
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
  && apt-get upgrade -y \
  && apt-get install -y \
                     apt-utils \
                     dbus-x11 \
                     dunst \
                     hunspell-en-us \
                     python3-dbus \
                     software-properties-common \
                     libx11-xcb1 \
                     gconf2 \
                     libgtk3.0 \
                     libxtst6 \
                     libnss3 \
                     libasound2 \
                     wget \
                     curl \
                     ca-certificates \
  && rm -rf /var/lib/apt/lists/* \
  && apt-get -y clean \
  && apt-get -y autoclean

WORKDIR /tmp

RUN wget "https://discordapp.com/api/download/canary?platform=linux&format=tar.gz" -O discord.tar.gz \
  && tar xvzf discord.tar.gz \
  && fc-cache -fv

WORKDIR /root

ENTRYPOINT rm -rf DiscordCanary/* && mv /tmp/DiscordCanary . && sh DiscordCanary/DiscordCanary
