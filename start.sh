#!/bin/sh

docker run --rm -it --name discord -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY --device /dev/snd -v /etc/localtime:/etc/localtime:ro -v /home/$USER/.discord-docker:/root/DiscordCanary quay.io/adrift/docker-discord

# override entrypoint for debugging
#docker run --rm -it --name discord --entrypoint /bin/bash -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY --device /dev/snd -v /etc/localtime:/etc/localtime:ro -v /home/$USER/.discord-docker:/root/DiscordCanary quay.io/adrift/docker-discord

